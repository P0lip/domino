/* jshint -W084 */
import { each, mayFail, tryCatch } from "./core.es6";

export class ExtendedPromise {
  constructor(func) {
    this.status = "pending";
    this.results = {
      resolve: [],
      reject: []
    };
    this.queue = {
      then: [],
      catch: []
    };

    tryCatch(() => {
      func(this.resolve.bind(this), this.reject.bind(this));
    }, ex => {
      this.reject(ex);
    });
  }

  then(callback) {
    if (this.status === "pending")
      this.queue.then.push(callback);
    else if (this.status === "fullfilled")
      callback.apply(null, this.results.resolve);
    return this;
  }

  catch(callback) {
    if (this.status === "pending")
      this.queue.catch.push(callback);
    else if (status === "rejected")
      callback.apply(null, this.results.reject);
    return this;
  }

  resolve(...args) {
    if (this.status === "pending") {
      this.status = "fullfilled";
      [].push.apply(this.results.resolve, args);
      if (this.queue.then.length) {
        let callback;
        while (callback = this.queue.then.shift())
          mayFail(callback, null, ...args);
        }
    }
  }

  reject(...args) {
    if (this.status === "pending") {
      this.status = "rejected";
      [].push.apply(this.results.reject, args);
      if (this.queue.catch.length) {
        let callback;
        while (callback = this.queue.catch.shift())
          mayFail(callback, null, ...args);
      }
    }
  }

  static all(promises) {
    return new ExtendedPromise((resolve, reject) => {
      let i = 0,
          isRejected = false;
      each(promises, promise => {
        promise
          .then(result => {
            i++;
            if (promises.length === i)
              resolve(result);
          })
          .catch(ex => {
            if (!isRejected) {
              reject(ex);
              isRejected = true;
            }
          });
      });
    });
  }

  static race(promises) {
    return new ExtendedPromise((resolve, reject) => {
      let isResolved = false,
          isRejected = false;
      each(promises, promise => {
        promise
          .then(result => {
            if (!isResolved && !isRejected) {
              resolve(result);
              isResolved = true;
            }
          })
          .catch(ex => {
            if (!isRejected) {
              reject(ex);
              isRejected = true;
            }
          });
      });
    });
  }

  static resolve(toResolve) {
    if (toResolve instanceof ExtendedPromise)
      return toResolve.resolve();
    if (typeof toResolve.then === "function") {
      const promise = new ExtendedPromise();
      toResolve.then(function(value) {
        promise.resolve(value);
      }, function(reason) {
        promise.reject(reason);
      });
      return promise;
    }
    const promise = new ExtendedPromise();
    promise.resolve(toResolve);
    return promise;
  }

  static reject(reason) {
    const promise = new ExtendedPromise();
    promise.reject(reason);
    return promise;
  }
}

export const Promise = window.Promise || class extends ExtendedPromise {
  constructor(func) {
    super(func);
  }

  then(callback) {
    if (this.status === "pending")
      this.queue.then.push(callback);
    else if (this.status === "fullfilled")
      callback.apply(null, this.results.resolve[0]);
    return this;
  }

  catch(callback) {
    if (this.status === "pending")
      this.queue.catch.push(callback);
    else if (status === "rejected")
      callback.apply(null, this.results.reject[0]);
    return this;
  }
};