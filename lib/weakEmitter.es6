/** @module WeakEmitter */
import { tryCatch } from "./core.es6";
import { Emitter } from "./emitter.es6";

/**
 * A "weaky" emitter wrapping around the normal emitter. Each method is inherited, yet there is one extra optional argument, that is any object to trigger the event on.
 *
 * @constructor
 * @requires module:emitter
 * @param {object|function} [target={}]   target - Any object and function excluding null, used as a default event target.
 * @param {boolean}         [queue=false] queue  - Indicates whether the events can be queue so that they can be used by further event listeners.
 *
 */

export function WeakEmitter(target = {}, queue = true) {
  if (!(this instanceof WeakEmitter))
    throw new Error("Failed to construct 'WeakEmitter': Please use the 'new' operator.");
  Emitter.call(this, target, queue);
}

/**
 * [proxyTarget description]
 * @param  {[type]} target  [description]
 * @param  {[type]} func    [description]
 * @param  {[type]} ...args [description]
 * @return {[type]}         [description]
 */
function proxyTarget(target, func, ...args) {
  const _target = this.target;
  this.target = target;
  let result = tryCatch(() => func.apply(this, args)); // Even though the methods derived from Emitter are fail-safe, we use the tryCatch method to make sure these functions will really never fail -> it's caused by the need to protect the target as if the function failed, we would lose the original target.
  this.target = _target;
  return result;
}

/**
 * @see module:emitter for the methods reference. They are basically the same with one exception - you can pinpoint the target.
 */

WeakEmitter.prototype.dequeue = function(target, type) {
  if (arguments.length === 1)
    [target, type] = [this.target, target];
  return proxyTarget.call(this, target, Emitter.prototype.dequeue, type);
};

WeakEmitter.prototype.delegate = function(target, type, listener) {
  if (arguments.length === 2)
    [target, type, listener] = [this.target, target, type];
  return proxyTarget.call(this, target, Emitter.prototype.delegate, type, listener);
};

WeakEmitter.prototype.emit = function(target, type, ...args) {
  if (typeof target === "string")
    [target, type, ...args] = [].concat.apply([{}, target, type], args); // tmp workaround due to the bug in babel.
  //  [target, type, ...args] = [this.target, target, type, ...args];
  return proxyTarget.call(this, target, Emitter.prototype.emit, type, ...args);
};

WeakEmitter.prototype.off = function(target, type, listener) {
  if (arguments.length === 2)
    [target, type, listener] = [this.target, target, type];
  return proxyTarget.call(this, target, Emitter.prototype.off, type, listener);
};

WeakEmitter.prototype.on = function(target, type, listener) {
  if (arguments.length === 2)
    [target, type, listener] = [this.target, target, type];
  return proxyTarget.call(this, target, Emitter.prototype.on, type, listener);
};

WeakEmitter.prototype.once = function(target, type, listener) {
  if (arguments.length === 2)
    [target, type, listener] = [this.target, target, type];
  return proxyTarget.call(this, target, Emitter.prototype.once, type, listener);
};

WeakEmitter.prototype.has = function(target, type, listener) {
  if (arguments.length === 2 && typeof target !== "object")
    [target, type, listener] = [this.target, target, type];
  return proxyTarget.call(this, target, Emitter.prototype.has, type, listener);
};

WeakEmitter.prototype.setMaxListeners = function(target, type, n) {
  if (arguments.length === 2 && typeof target !== "object")
    [target, type, n] = [this.target, target, n];
  return proxyTarget.call(this, target, Emitter.prototype.setMaxListeners, type, n);
};

WeakEmitter.prototype.history = function(target, type, n) {
  if (arguments.length === 2 && typeof target !== "object")
    [target, type, n] = [this.target, target, n === undefined ? "first" : n];
  return proxyTarget.call(this, target, Emitter.prototype.history, type, n);
};

WeakEmitter.prototype.promisify = function(target, type, queue) {
  if (arguments.length === 2 && typeof target !== "object")
    [target, type, queue] = [this.target, target, type];
  return proxyTarget.call(this, target, Emitter.prototype.promisify, type, queue === undefined ? true : queue);
};