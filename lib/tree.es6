import { each } from "./core.es6";

export class Tree {
  constructor(name, root) {
    this.root = root;
    this.document = document.createDocumentFragment(); // isn't it unused?
    this.current = null;
  }

  get path() {
    if (!this.current)
      return this;
    const path = [];

    let parent = this.current.parentElement;
    while (parent) {
      path.push(parent);
      parent = parent.parentElement;
    }

    path.reverse(); // reverse, because the highest are at the end as of now
    path.push(this.current);

    let child = this.current.firstElementChild;
    while (child) {
      path.push(child);
      child = child.firstElementChild;
    }
    return path;
  }

  navigate(n) {
    const path = this.path;
    if (path[n])
      this.current = path[n];
    return this;
  }

  top() {
    return this.navigate(0);
  }

  bottom() {
    return this.navigate(this.history.length - 1);
  }

  up() {
    const index = this.index;
    if (index > 0)
      this.current = this.path[index-1];
    return this;
  }

  down() {
    const index = this.index,
          path = this.path;
    if (index < path.length-1)
      this.current = this.path[index+1];
    return this;
  }

  get index() {
    return this.path.indexOf(this.current);
  }

  attach() {
    this.root.appendChild(this.document);
  }

  static createElement(tag, { parent, events, textContent, html, data, css, classes, ...attrs } = {}) {
    const elem = document.createElement(tag);

    if (attrs)
      each(attrs, (attr, attrValue) => {
        elem.setAttribute(attr, attrValue);
      });

    if (css)
      each(css, (rule, ruleValue) => {
        elem.style.setProperty(rule, ruleValue);
      });

    if (classes && classes.length)
      elem.classList.add.apply(elem.classList, Array.isArray(classes) ? classes : classes.split(","));

    if (textContent && textContent.length)
      elem.textContent = textContent;

    if (html && html.length)
      elem.insertAdjacentHTML("afterbegin", html);

    if (data)
      each(data, (data, dataValue) => {
        elem.dataset[data] = dataValue;
      });

    if (events)
      each(events, (event, listeners) => {
        if (typeof listeners === "function")
          elem.addEventListener(event, listeners, false);
        else
          each(listeners, listener => {
            elem.addEventListener(event, listener, false);
          });
      });

    if (typeof parent === "string") {
      let parent = document.querySelector(parent);
      if (parent)
        parent.appendChild(elem);
    } else if (parent && parent.appendChild)
      parent.appendChild(elem);

    return elem;
  }

  createElement() {
    return Tree.createElement.apply(null, arguments);
  }

}