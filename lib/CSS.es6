"use strict";
import { contains. merge, generateGUID } from "./core.es6";
import $ from "./DOM.es6";
import { events, CSS as _CSS } from "./feature.es6";
const { isFeatureSupported, CSSPrefix } = _CSS,
      supportedUnprefixed = $.filter(win.getComputedStyle(doc.docElement, null), rule => contains(rule, CSSPrefix)),
      transitionEnd = events.transition.end,
      GUID = generateGUID(1);

if (transitionEnd)
  var transitionEvent = $.createEvent(transitionEnd);

export const easings = {
  ease: "ease",
  linear: "linear",
  "ease-in": "ease-in",
  "ease-out": "ease-out",
  "ease-in-out": "ease-in-out",

  easeInSine: "cubic-bezier(0.47, 0, 0.745, 0.715)",
  easeOutSine: "cubic-bezier(0.39, 0.575, 0.565, 1)",
  easeInOutSine: "cubic-bezier(0.445, 0.05, 0.55, 0.95)",

  easeInQuad: "cubic-bezier(0.55, 0.085, 0.68, 0.53)",
  easeOutQuad: "cubic-bezier(0.25, 0.46, 0.45, 0.94)",
  easeInOutQuad: "cubic-bezier(0.455, 0.03, 0.515, 0.955)",

  easeInCubic: "cubic-bezier(0.55, 0.055, 0.675, 0.19)",
  easeOutCubic: "cubic-bezier(0.215, 0.61, 0.355, 1)",
  easeInOutCubic: "cubic-bezier(0.645, 0.045, 0.355, 1)",

  easeInQuart: "cubic-bezier(0.895, 0.03, 0.685, 0.22)",
  easeOutQuart: "cubic-bezier(0.165, 0.84, 0.44, 1)",
  easeInOutQuart: "cubic-bezier(0.77, 0, 0.175, 1)",

  easeInQuint: "cubic-bezier(0.755, 0.05, 0.855, 0.06)",
  easeOutQuint: "cubic-bezier(0.23, 1, 0.32, 1)",
  easeInOutQuint: "cubic-bezier(0.86, 0, 0.07, 1)",

  easeInExpo: "cubic-bezier(0.95, 0.05, 0.795, 0.035)",
  easeOutExpo: "cubic-bezier(0.19, 1, 0.22, 1)",
  easeInOutExpo: "cubic-bezier(1, 0, 0, 1)",

  easeInCirc: "cubic-bezier(0.6, 0.04, 0.98, 0.335)",
  easeOutCirc: "cubic-bezier(0.075, 0.82, 0.165, 1)",
  easeInOutCirc: "cubic-bezier(0.785, 0.135, 0.15, 0.86)",

  easeInBack: "cubic-bezier(0.6, -0.28, 0.735, 0.045)",
  easeOutBack: "cubic-bezier(0.175, 0.885, 0.32, 1.275)",
  easeInOutBack: "cubic-bezier(0.68, -0.55, 0.265, 1.55)",

  get random() {
    const props = Object.keys(this);
    return this[props[parseInt(Math.random()*props.length)]];
  }
};

const boxModel = function(elem, prop) {
  if (!prop || !$.isElementNode(elem))
    return {};
  const { getPropertyValue } = win.getComputedStyle(elem, null);
  return {
    top: getPropertyValue(`${prop}-top`),
    right: getPropertyValue(`${prop}-right`),
    bottom: getPropertyValue(`${prop}-bottom`),
    left: getPropertyValue(`${prop}-left`)
  };
};

const dimensions = (elem, typeForwin, typeForElement, typeFordoc) => elem === win ? elem[typeForwin] : (elem === doc ? doc.docElement : elem)[typeFordoc ? typeFordoc : typeForElement];

export const forceReflow = elem => elem.outerWidth && elem.outerHeight;

$.animate = isFeatureSupported("transition", "all 1s", true) ? function(elem, props, { duration = 750, easing = easings.random, reflow = false } = {}) {
  if (reflow)
    forceReflow(elem);
} : function() {

};

$.prototype.animate =  ? function(props) {
  if (reflow)
    this.forceReflow();

  const transition = Object.keys(props).join(",") + " " + easing + " " + duration/1000 + "s"; // TODO: add prefix

  $.data(elem, `${GUID}transition`, {
    transition,
    easing,
    duration
  });

  this.off(transitionEnd);
  this.on(transitionEnd, function callback(e) {
    if (this !== e.target)
      return;
    self.off(transitionEnd, callback);
    self.removeCSS("transition");
    empty($.data(elem, `${GUID}transition`));

  });
  return this.css(merge({
    transition
  }, props));
  } : function(props, callback) {
  if (typeof callback === "function")
    callback();
  this.trigger(transitionEvent);
  return this.css(props);
};

$.prototype.stop = function() {
  this.removeCSS();
};

// finish() {
//
//   },

$.prototype.fadeIn = function(setup) {
  this.show();
  this.css("opacity", "0");
  return this.animate({
    opacity: "1"
  }, merge(setup, { reflow: true });
};

$.prototype.fadeOut = function(setup) {
  this.show();
  this.css("opacity", "1");
  return this.animate({
    opacity: "0"
  }, merge(setup, { reflow: true });
};

$.prototype.toggleUp = function(setup) {
  this.show();
  return this.animate({
    height: "0"
  }, setup);
};

$.prototype.toggleDown = function(setup) {
  this.show();
  this.reflow();
  return this.animate({ // TODO: single elem.
    //height: "0"
  }, merge(setup, { reflow: true }));
};

  revert(withTransition) { // with anim or not
    if (!this.propsToRevert)
      return;
    this.removeCss(this.propsToRevert);
    while(this.propsToRevert.length) // save in object
      this.propsToRevert.pop();
    return this;
  },

  css(styleProp, styleVal) {
    //this.propsToRevert = Object.keys(_props);
    const isObjectLiteral = this.isObjectLiteral(styleProp);
    return isObjectLiteral || (typeof styleProp === "string" && styleVal !== undefined) ? this.each(elem => {
        let setStyles = (styleProp, styleVal) => { // check dirty like margin padding border etc etc
          const props = {},
              style = elem.style,
              prefixedProp = CSSPrefix + styleProp;

          if (supportedUnprefixed.indexOf(prefixedProp) !== -1) {
            if (typeof styleVal === "number")
              style.setProperty(prefixedProp, `${styleVal}px`);
            style.setProperty(prefixedProp, styleVal);
            props[prefixedProp] = styleProp;
          }
          props[styleProp] = styleProp;
          if (typeof styleVal === "number")
            style.setProperty(prefixedProp, `${styleVal}px`);
          style.setProperty(styleProp, styleVal);
        };
        if (isObjectLiteral)
          Core.each(styleProp, setStyles);
        else
          setStyles(styleProp, styleVal);
      }) : win.getComputedStyle(this.iswin || this.isdoc ? this.get().docElement : this.get(), null).getPropertyValue(styleProp);
  },
  removeCSS(styleProps) {
    const isArray = Array.isArray(styleProps);
    return this.each(elem => {
      let removeStyles = (styleProp, styleVal) => {
        if (supportedUnprefixed.indexOf(CSSPrefix + styleProp) !== -1)
          elem.style.removeProperty(CSSPrefix + styleProp);
        elem.style.removeProperty(styleProp);
      };
      if (isArray)
        Core.each(styleProps, removeStyles);
      else
        removeStyles(styleProps);
    });
  },

  hide() {
    return this.css("display", "none");
  },
  show() {
    this.removeCSS("display");
  },
  toggle() {
    this.each(elem => elem.style.display === "none" ? elem.style.display = "" : elem.style.display = "none");
  },


  // Dimensions
  get border() {
    return boxModel(this.first(), "border");
  },
  get margin() {
    return boxModel(this.first(), "margin");
  },
  get padding() {
    return boxModel(this.first(), "padding");
  },
  get offset() { // bottom, right
    const elem = this.first();
    return this.iswin ? {
      top: elem.pageYOffset,
      left: elem.pageXOffset
    } : {
      parent: elem.offsetParent,
      top: elem.offsetTop,
      left: elem.offsetLeft,
      width: elem.offsetWidth,
      height: elem.offsetHeight
    };
  },
  scrollBy(x, y) {
    if (x !== 0 || y !== 0)
      win.scrollBy(x + this.win.offset.left, y + this.win.offset.top);
    return this;
  },
  scrollTop(scroll) {
    if (scroll === undefined)
      return this.win.offset.top;
    win.scrollTo(0, scroll|0);
  },
  scrollLeft(scroll) {
    if (scroll === undefined)
      return this.win.offset.left;
    win.scrollTo(scroll|0, 0);
  },


  get innerHeight() { // padding
    return this.dimensions(this.first(), "innerHeight", "clientHeight");
  },
  height(height) { // padding, scrollBar, border
    return height ? this.css("height", typeof height === "number" ? `${height}px` : height) : this.dimensions(this.get(), "innerHeight", "offsetHeight", "clientHeight");
  },
  get outerHeight() { // padding, scrollBar, border, margin
    let matched = this.dimensions(this.get(), "outerHeight", "offsetHeight", "clientHeight");
    return this.iswin ? matched : (() => {
      const margins = this.margin,
            reg = /\d*(?=[\.\w]|)/;
      return matched + (+reg.exec(margins.top)[0] + (+reg.exec(margins.bottom)[0]));
    })();
  },

  get innerWidth() { // padding
    return dimensions(this.get(), "innerWidth", "clientWidth");
  },
  width(width) { // padding, scrollBar, border
    return width ? this.css("width", typeof width === "number" ? `${width}px` : width) : this.dimensions(this.get(), "innerWidth", "offsetWidth", "clientWidth");
  },
  get outerWidth() { // padding, scrollBar, border, margin
    let matched = dimensions(this.get(), "outerHeight", "offsetWidth", "clientWidth");
    return this.iswin ? matched : (() => {
      const margins = this.margin,
            reg = /\d*(?=[\.\w])/;
      return matched + (+reg.exec(margins.left)[0]) + (+reg.exec(margins.right)[0]);
    })();
  },

  visible(selector) { // FALSE POSITIVE
    return this.push(this.isVisible(selector, true));
  },
  isVisible(selector, returnElems) { // check for display
    let visible = this.filter(elem => {
      const offset = (new this.constructor(elem)).offset;
      return (offset.top + offset.height) <= this.win.innerHeight && (offset.left + offset.width) <= this.win.innerWidth;

      //console.log(offset.top + offset.height);
     // return (offset.top + offset.height >= scrollTop - halfHeight && offset.top <= scrollTop + halfHeight) && (offset.left + offset.width >= scrollLeft - halfWidth && offset.left <= scrollLeft + halfWidth);
    });
    if (selector)
      visible = this.filterElems(visible, selector);
    return returnElems ? visible : visible.length > 0;
  }
});
