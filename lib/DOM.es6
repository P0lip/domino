import { bind, contains, destruct, each, extract, every, empty, filter, isObjectLiteral, map, merge, generateGUID, SimplePromise, slice, unique, tryCatch, type } from "./core.es6";
import { _WeakMap } from "./collections.es6";
import { Tree } from "./tree.es6";
import { Emitter } from "./emitter.es6";
import { WeakEmitter } from "./weakEmitter.es6";
import { request, get, post, getJSON, getHTML, parseJSON, parseHTML, parseXML } from "./xhr.es6";
import { log } from "./console.es6";

const A = Array.prototype,
      regex = {
        space: /\s+/,
        selector: /[^A-Za-z0-9_\.\-]/,
        css: /[>.+~:]/ // Incomplete
      },
      GUID = generateGUID(1),
      lazyListeners = new _WeakMap(),
      dataContainer = new _WeakMap(); // shared data container

// Vendor prefixes section
/* jshint -W030 */
(function (elemProto) {
  elemProto.matches || (elemProto.matches = elemProto.mozMatchesSelector || elemProto.msMatchesSelector || elemProto.oMatchesSelector || elemProto.webkitMatchesSelector);
  elemProto.closest || (elemProto.closest = function(selector) {
    let elem = this;
    while (elem) {
      if (this.matches(selector))
        break;
      elem = elem.parentNode;
    }
    return elem;
  });
  elemProto.remove || (elemProto.remove = function() {
    this.parentElement.removeChild(this);
  });
  win.MutationObserver || (win.MutationObserver = win.WebKitMutationObserver);
})(Element.prototype);
/* jshint +W030 */


const HTMLCollectionRegExp = /HTML(.+|)Collection/;

const sortDOM = elems => elems.sort((elemA, elemB) => elemA.compareDocumentPosition(elemB) === 4 ? -1 : 1);
const dir = (elems, dir, until) => unique(map(elems, elem => {
  while ((elem = elem[dir])) {
    if (until && is(elem, until))
      break;
    return elem;
  }
}));

const isCollection = elems => elems && elems.length !== undefined && contains(type(elems), HTMLCollectionRegExp);
const isNodeList = elems => elems instanceof NodeList || contains(type(elems), "NodeList");
const areNodes = elems => isNodeList(elems) || isCollection(elems);

const helpers = {
  split: (obj, split = regex.space) => typeof obj === "string" ? contains(obj, " ") ? obj.split(split) : [obj] : obj,
  isCustomSelector: selector => contains(selector, " ") || contains(selector, ",") || selector.lastIndexOf(".") !== 0 || selector.lastIndexOf("#") !== 0 || contains(selector, regex.css)
};

export const $ = (...args) => new DOMino(...args);

export default $;

function DOMino(selector, context = document, instance = true) {
  if (!selector)
    return this;

  if (typeof selector === "function") {
    $.ready(selector);
    this.length = 0;
  } else {
    let elems;
    if (typeof selector === "string") {
      let firstSign = selector[0],
          multiple = helpers.isCustomSelector(selector);

      if (multiple) // it could be written using ternary operator, but it wouldn't be as readable as the following stuff
        elems = context.querySelectorAll(selector);
      else if (firstSign === "#")
        elems = document.getElementById(selector.slice(1));
      else if (firstSign === ".")
        elems = context.getElementsByClassName(selector.slice(1));
      else if (doc[selector] && $.isElementNode(doc[selector]))
        elems = doc[selector];
      else
        elems = context.getElementsByTagName(selector);
    } else if (selector instanceof DOMino || selector.length && selector !== win && selector !== doc)
      elems = slice(selector);
    else
      elems = selector;
    if ("length" in elems && elems !== win && elems !== doc) {
      if (elems.length)
        this.push.apply(this, elems);
      else
        this.length = 0;
    } else {
      this[0] = elems;
      this.length = 1;
    }
  }

  this.context = this[0] === win ? win : context;

  if (instance !== false)
    this.old = typeof instance === "object" ? instance : this;

  this.emitter = new Emitter(this, true);
}

DOMino.prototype = Object.create(Array.prototype);

DOMino.prototype.constructor = DOMino;

$.xhr = $.ajax = request;

$.get = get;

$.post = post;

$.getJSON = getJSON;

$.getHTML = getHTML;

$.createElement = Tree.createElement;

$.Tree = Tree;

$.isElementNode = elem => elem && elem.nodeType === 1; // REFERENCE: https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType

$.isNode = elem => elem && elem.nodeType !== undefined;

$.is = function(elem, toMatch) {
  return !!($.isElementNode(elem) ? typeof toMatch === "string" ? elem.matches(toMatch) : elem.isEqualNode(toMatch) : false);
};

$.lazy = function(target, evt, callback) {
  if (!$.events.has(target, evt))
    target.addEventListener(evt, function(e) {
      $.events.emit(target, evt, e);
    }, false);
  const boundCallback = bind(callback, target);
  $.events.on(target, evt, boundCallback);
  const off = bind($.events.off, $.events, target, evt, boundCallback);
  lazyListeners.set(callback, off);
  return off;
};

$.lazy.off = callback => {
  if (lazyListeners.has(callback)) {
    lazyListeners.get(callback)();
    return true;
  }
  return false;
};

$.ready = function(callback) {
  if (doc.readyState === "complete")
    callback(doc);
  else
    return $.lazy(doc, "DOMContentLoaded", callback);
};

export const ready = new Promise(resolve => {
  $.ready(function() {
    resolve(doc);
  });
});

$.scroll = callback => $.lazy(win, "scroll", callback);

$.resize = callback => $.lazy(win, "resize", callback);

$.createEvent = function() {
  const Constructor = tryCatch(function() {
    new Event("e");
    return Event;
  }, () => function(name) {
    const event = doc.createEvent("Event");
    event.initEvent(name, true, true);
    return event;
  });
  return type => new Constructor(type);
}();

$.events = new WeakEmitter($, true);

$.data = function(elem, key, val) {
  if (typeof elem === "string")
    elem = document.querySelector(elem);

  if (!val)
    return dataContainer.has(elem) ? dataContainer.get(elem)[key] : undefined;

  let data = { [key]: val };

  dataContainer.set(elem, dataContainer.has(elem) ? merge(dataContainer.get(elem), data) : data);
};

$.onDetached = function(elems, callback) {
  each(elems.length ? elems : [elems], elem => {
    const parentNode = elem.parentNode;
    if (parentNode) {
      let nextSibling = elem.nextSibling;
      parentNode.removeChild(elem);
      callback(elem);
      parentNode.insertBefore(elem, nextSibling);
    }
  });
};

$.parseHTML = parseHTML;

$.parseJSON = parseJSON;

$.parseXML = parseXML;

DOMino.prototype.then = function(callback) {
  if (this.currentPromise)
    this.currentPromise.then(callback);
  return this;
};

DOMino.prototype.catch = function(callback) {
  if (this.currentPromise)
    this.currentPromise.catch(callback);
  return this;
};

DOMino.prototype.promise = function() {
  this.currentPromise = new SimplePromise((resolve, reject) => {
    this.currentPromise.resolve = resolve;
    this.currentPromise.reject = reject;
  });
};

DOMino.prototype.pushStack = function(elems, _unique = true) {
  /*jshint -W030*/
  if (_unique && elems.length > 1)
    elems = unique([].slice.call(elems));
  else if (elems && elems.length === undefined)
    elems = [elems];
  /*jshint +W030 */

  if (this.old || !elems) {
    if (elems)
      return new this.constructor(elems, this.context, this);
    return this;
  }

  while (this.length)
    this.pop();

  if (elems)
    this.push.apply(this, elems);
  return this;
};

$.each = DOMino.prototype.each = function(callback, _callback) {
  if (this instanceof DOMino) {
    if (!this.isEmpty)
      each(this, (elem, n, elems) => {
        callback(elem, n, elems);
      });
    return this;
  }
  each(callback, _callback);
};

DOMino.prototype.map = function(callback) {
  return this.length ? this.pushStack(extract(map(this, (elem, n, elems) => callback(elem, n, elems)))) : this;
};

DOMino.prototype.filter = function(rules) {
  return this.length ? this.pushStack(filter(this, typeof rules === "string" ? elem => elem.matches(rules) : $.isNode(rules) ? elem => elem === rules : elem => contains(rules, elem))) : this;
};

$.win = DOMino.prototype.win = new DOMino(window);

$.doc = DOMino.prototype.doc = new DOMino(document);

DOMino.prototype.addClass = function(classes) {
  if (!classes || !classes.length) return this;

  let splitClasses = helpers.split(classes);
  return this.each(elem => {
    each(splitClasses, _class => elem.classList.add(_class));
  });
};

DOMino.prototype.hasClass = function(classes) {
  if (!classes || !classes.length) return this;

  let splitClasses = helpers.split(classes),
      matched = false;

  every(this, elem => {
    let ret = true;
    every(splitClasses, _class => ret = !elem.classList.contains(_class));
    matched = !ret;
    return !ret;
  });
  return matched;
};

DOMino.prototype.toggleClass = function(classes, toggle) {
  if (!classes || !classes.length) return this;

  if (toggle === undefined) {
    let splitClasses = helpers.split(classes);
    return this.each(elem => {
      each(splitClasses, _class => elem.classList.toggle(_class));
    });
  }
  return toggle ? this.addClass(classes) : this.removeClass(classes);
};

DOMino.prototype.removeClass = function(classes) {
  if (!classes || !classes.length) return this;

  let splitClasses = helpers.split(classes);
  return this.each(elem => {
    each(splitClasses, _class => elem.classList.remove(_class));
  });
};

DOMino.prototype.attr = function(attr, attrVal) {
  if (!attr)
    return this;
  if (typeof attr === "string" && attrVal === undefined)
    return this.get().getAttribute ? this.get().getAttribute(attr) : undefined;
  if (isObjectLiteral(attr))
    this.each(elem => {
      each(attr, (attr, val) => {
        if (elem.setAttribute)
          elem.setAttribute(attr, val); // TODO: validate key and values to prevent InvalidCharError
      });
    });
  else if (typeof attr === "string")
    this.each(elem => {
      if (elem.setAttribute)
        elem.setAttribute(attr, ""+attrVal);
    });
  return this;
};

DOMino.prototype.removeAttr = function(attrs) {
  if (attrs) {
    let _attrs = contains(attrs, regex.space) ? [attrs] : attrs.split(regex.space);
    this.each(elem => {
      each(_attrs, attr => {
        elem.removeAttribute(attr);
      });
    });
  }
  return this;
};

DOMino.prototype.off = function(event, callback) {
  if (typeof event === "string") {
    let events = helpers.split(event);
    this.each(elem => {
      let eventsData = $.data(elem, `${GUID}events`);
      each(events, event => {
        let events = eventsData[event];
        if (!events)
          return;
        if (typeof callback === "function") {
          elem.removeEventListener(event, callback, false);
          let index = events.indexOf(callback);
          while (index !== -1) {
            events.splice(index, 1);
            index = events.indexOf(callback);
          }
        } else {
          each(events, callback => {
            elem.removeEventListener(event, callback, false);
          });
          empty(events);
        }
      });
    });
  }
  return this;
};

DOMino.prototype.on = function(event, callback) {
  if (typeof event === "string" && typeof callback === "function") {
    let events = helpers.split(event);
    this.each(elem => {
      let eventsData = $.data(elem, `${GUID}events`);
      if (!eventsData) {
        eventsData = {};
        $.data(elem, `${GUID}events`, eventsData);
      }
      each(events, event => {
        let events = eventsData[event];
        if (!events)
          eventsData[event] = [callback];
        else
          eventsData[event].push(callback);
        elem.addEventListener(event, callback, false);
      });
    });
  }
  return this;
};

DOMino.prototype.once = function(event, callback) {
  if (typeof event === "string" && typeof callback === "function") {
    let events = helpers.split(event);
    let self = this;
    this.each(elem => {
      each(events, event => {
        elem.addEventListener(event, function _callback(e) {
          this.removeEventListener(e.type, _callback, false);
          callback.call(this, e);
          let eventsContainer = self.data(this, `${GUID}events`)[event];
          every(eventsContainer, event => {
            if (event.callback === _callback) {
              eventsContainer.splice(eventsContainer.indexOf(event), 1);
              return false;
            }
            return true;
          });
        }, false);
      });
    });
  }
  return this;
};

DOMino.prototype.one = function(event, callback) {
  const self = this;
  let events = helpers.split(event);
  function _callback(e) {
    callback.call(this, e);
    each(events, event => {
      self.off(event, _callback);
    });
  }
  each(events, event => {
    this.once(event, _callback);
  });
};

DOMino.prototype.trigger = function(event) {
  if (typeof event === "string")
    event = this.createEvent(event);
  else if (!this.isEvent(event))
    return this;
  return this.each(elem => elem.dispatchEvent(event));
};

/**
 * Gets HTML markup from the first element or injects the given one.
 *
 * @param  {string} html - html markup to inject
 * @return {string|DOMino}
 */
DOMino.prototype.html = function(html) {
  if (html === undefined)
    return this.get().innerHTML;
  if (typeof html === "string")
    this.detach(elem => {
      while (elem.firstChild)
        elem.removeChild(elem.firstChild);
      if (html.length)
        elem.insertAdjacentHTML("afterbegin", html);
    });
  return this;
};

/**
 * Injects text or get the text content from the first element on stack.
 *
 * @param  {string} text - text to inject
 * @return {string|DOMino}
 */
DOMino.prototype.text = function(text) {
  if (text === undefined)
    return this.get().textContent;
  if (typeof text === "string")
    this.each(elem => {
      elem.textContent = text.length ? text : "";
    });
  return this;
};

DOMino.prototype.detach = function(callback) {
  return this.each(elem => {
    $.onDetached(elem, callback);
  });
};

DOMino.prototype.empty = function() {
  return this.detach(elem => {
    while (elem.firstChild)
      elem.firstChild.remove();
  });
};

DOMino.prototype.remove = function(selector) {
  return this.push(this.filter(elem => {
    if (!selector || (selector && elem.matches(selector))) {
      elem.remove();
      return false;
    }
    return true;
  }));
};

DOMino.prototype.disconnect = function() {
  return !win.MutationObserver ? this : this.each(elem => {
    let mutations = $.data(elem, `${GUID}mutations`);
    if (!mutations)
      return;
    each(mutations => mutations.disconnect());
    empty(mutations);
  });
};

DOMino.prototype.observe = win.MutationObserver ? function(callback, setup = { childList: true }) {
  log.info("NOTE: this feature is supported only in modern browsers.");
  this.each(elem => {
    let observer = new MutationObserver(callback);
    observer.observe(elem, setup);
    if ($.data(elem, `${GUID}mutations`))
      $.data(elem, `${GUID}mutations`).push(observer);
    else
      $.data(elem, `${GUID}mutations`, [observer]);
  });
  return this;
} : function() {
  log.error("This feature is not supported in the current browser.");
  return this;
};

DOMino.prototype.end = function() {
  return this.old;
};

DOMino.prototype.get = function(index) {
  return this[index ? index > 0 ? index : (this.length+index) : 0];
};

DOMino.prototype.eq = function(index = 0) {
  return this.pushStack(this.get(index));
};

DOMino.prototype.first = function() {
  return this.eq();
};

DOMino.prototype.last = function() {
  return this.eq(-1);
};

DOMino.prototype.index = function(elem) {
  return elem ? this.indexOf(elem) : this.parent().children().indexOf(elem);
};

DOMino.prototype.exclude = function(selectorOrElems) {
  return this.slice().filter(selectorOrElems, elem => this.splice(this.indexOf(elem), 1));
};

DOMino.prototype.include = function(selectorOrElems) {
  const elems = new this.constructor(selectorOrElems);
  return elems.isWindow || elems.isDocument ? this : (() => {
    this.each(elems.elems, elem => this.elems.push(elem));
    this.sort();
    return this.old ? new this.constructor(this.elems) : this;
  })();
};

DOMino.prototype.matches = function(match) {
  return this.push(this.map(elem => this.is(elem, match)));
};

DOMino.prototype.find = function(selector) {
  const multipleSelector = helpers.isCustomSelector(selector),
        result = [];
  let action;
  if (multipleSelector)
    action = "querySelectorAll";
  else if (selector[0] === "#")
    return this.pushStack(doc.getElementById(selector.slice(1)));
  else if (selector[0] === ".") {
    action = "getElementsByClassName";
    selector = selector.splice(1);
  } else if (doc[selector] && $.isElementNode(doc[selector]))
    return this.pushStack(doc[selector]);
  else
    action = "getElementsByTagName";
  this.each(elem => {
    A.push.apply(result, elem[action](selector));
  });
  return this.pushStack(result, true);
};

DOMino.prototype.closest = function(selector) {
  return this.push(this.map(elem => elem.closest(selector)));
};

DOMino.prototype.siblings = function(filter) {
  const matched = dir("previousElementSibling").concat(dir("nextElementSibling"));
  return this.push(filter ? this.filter(matched, filter) : matched);
};

each({
  firstChild: "firstElementChild",
  lastChild: "lastElementChild",
  next: "nextElementSibling",
  prev: "previousElementSibling",
  children: "children",
  parent: "parentElement"
}, function(methodName, methodFunc) {
    DOMino.prototype[methodName] = function(filter) {
      let matched = this.map(elem => elem[methodFunc]);
      return this.pushStack(filter && matched.length ? this.filterStack(matched, filter) : matched, true);
    };
});

each({
  prevAll: "previousElementSibling",
  nextAll: "nextElementSibling",
  parents: "parentElement"
}, function(methodName, methodFunc) {
  DOMino.prototype[methodName] = function(filter) {
    let matched = dir(methodFunc);
    return this.pushStack(filter && matched.length ? this.filterStack(matched, filter) : matched, true);
  };
});

each({
  prevUntil: "previousElementSibling",
  nextUntil: "nextElementSibling",
  parentsUntil: "parentElement"
}, function(methodName, methodFunc) {
  DOMino.prototype[methodName] = function(filter) {
    let matched = [];
    this.each(elem => {
      while (elem[methodFunc] && !this.is(elem, filter))
        matched.push(elem[methodFunc]);
    });
    return matched;
  };
});

Object.defineProperties(DOMino.prototype, {
  isWindow: {
    get() {
      return this.length === 1 && this.get() === win;
    }
  },
  isDocument: {
    get() {
      return this.length === 1 && this.get() === doc;
    }
  },
  isEmpty: {
    get() {
      return !this.length;
    }
  }
});