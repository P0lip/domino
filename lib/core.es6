/**
 * @module core
 * Module provides methods used across all the files. Some of the included functions are just an alias for built-in functions in browser.
 * IE: support from version 9.
 */
export { ExtendedPromise as SimplePromise } from "./promises.es6"; // for backwards compatiblity

import { log } from "./console.es6";

const A = Array,
      O = Object,
      F = Function;
/**
 * Generates a random string. It's used internally by WeakMap/WeakSet shim.
 * NOTE: // It returns a proper GUID string when strength is equal to true.
 *
 * @function
 * @param  {number|boolean} strength - the amount of iteration. If true is passed it returns a proper GUID.
 * @return {string} random string
 */
export const generateGUID = (strength = 3) => {
  if (typeof strength === "number") {
    const str = new Array(strength);
    while (strength)
      str[--strength] = Math.random().toString(36).substring(2, 15);
    return str.join("");
  } else
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
      const random = Math.random()*16|0;
      return (c === "x" ? random : (random&0x3|0x8)).toString(16);
    });
};

export const generateRandomChar = () => {
  const guid = generateGUID(1);
  let char = guid[0];
  let i = 0;
  while (!isNaN(+char)) {
    char = guid[i++];
  }
  return char;
};

export const replaceLetters = word => map(word, (sign, n, arr) => isNaN(+sign) ? arr.charCodeAt(n) : +sign).join("");

/**
 * Creates something similar to generators.
 *
 * @param  {iterable} - iterable object
 * @return {object} iterator
 */
export function createSimpleIterator(iterable) {
  const slicedIterable = slice(iterable);
  let count = 0;
  return {
    next() {
      let value;
      if (count < slicedIterable.length)
        value = slicedIterable[count];
      return {
        done: count++ === slicedIterable.length,
        value
      };
    }
  };
}

/* jshint -W103 */
export const isObjectLiteral = obj => typeof obj !== "object" || obj === null ? false : (O.getPrototypeOf ? O.getPrototypeOf(obj) : obj.__proto__) === O.prototype;
/* jshint +W103 */

/**
 * [O description]
 * @param {[type]} obj [description]
 */
export const isObject = obj => obj !== null && typeof obj === "object" && obj === O(obj);

/**
 * [isArray description]
 * @param  {[type]}  obj [description]
 * @return {Boolean}     [description]
 */
export const isEmptyObject = obj => obj === null || typeof obj === "undefined" ? true : typeof obj === "string" || A.isArray(obj) ? obj.length === 0 : (isObjectLiteral(obj) ? O.getOwnPropertyNames(obj) : O.keys(obj)).length === 0;

/**
 * [description]
 * @param  {[type]} obj   [description]
 * @param  {[type]} name  [description]
 * @param  {[type]} value [description]
 * @return {[type]}       [description]
 */
export const definePrivateProperty = (obj, name, value) => O.defineProperty(obj, name, { // NOTE: It won't create a real private property like in C languages. In JS there are no private properties. This just returns a non-enumerable and non-configurable property.
        configurable: false,
        enumerable: false,
        writable: true,
        value
      });

/**
 * Detect the type of given object.
 *
 * @param   {any}     obj - any primitive type
 * @returns {string}  type
 */
export const type = function() {
  const shrink = /\s(\w+)/;
  return obj => shrink.exec({}.toString.call(obj))[1];
}();


// NOTE: Methods which help working with all objects having indices and the length property, so it means it will work with strings and objects.
export function splice(arr, elem) {
  if (!arr.indexOf || !arr.splice)
    return;
  const index = arr.indexOf(elem);
  if (index !== -1)
    arr.splice(index, 1);
}

/**
 * Breakable loop over any object literal (some other objects are supported aswell, but they must be array-like - they must contain indices and the length property), array or string.
 *
 * @param {object|object[]|string} - object to iterate on
 * @param {function} callback - function called with iterating element
 * @param {boolean} [nonenumerable] - a switch allowing to iterate through non-enumerable properties
 */
export function every(obj, callback, nonenumerable) {
  if ((typeof obj === "object" || typeof obj === "string") && typeof callback === "function") {
    if (obj.length !== undefined) {
      for (let i = 0; i < obj.length; i++)
        if (callback(obj[i], i, obj) === false)
          return;
    } else if (typeof obj === "function" || isObject(obj)) {
      for (let i = 0, keys = nonenumerable ? O.getOwnPropertyNames(obj) : O.keys(obj); i < keys.length; i++)
        if (callback(keys[i], obj[keys[i]], i, obj) === false)
          return;
    } else
      callback(obj, 0, obj);
  }
}

/**
 * Check if the given object contains the argument - either in object literal or array or string.
 *
 * @param   {string|object[]|object} obj   - the argument to make a check on
 * @param   {string}                 value - the value to look for
 * @returns {boolean}
 */
export const contains = (obj, value) => obj && obj.indexOf ? obj.indexOf(value) !== -1 : value instanceof RegExp ? value.test(obj) : value in obj;

/**
 * Loop over a string, an object or an array.
 *
 * @param {string|object[]|object} obj             - the argument to perform loop on
 * @param {function}               callback        - the callback called with the following arguments in order: value, iteration count, the object itself
 * @param {boolean}                [nonenumerable] - a switch allowing to iterate through non-enumerable properties
 */
export function each(obj, callback, nonenumerable) {
  every(obj, (...args) => {
    callback(...args);
    return true;
  }, nonenumerable);
}

/**
 * Flatten the array.
 *
 * @param   {object[]} obj - the array to flatten
 * @returns {object[]} flattened array
 */
export function extract() {
  function _extract(from, to = []) {
    each(from, elem => elem.length !== undefined ? to.concat(_extract(elem, to)) : to.push(elem));
    return to;
  }
  return _extract(proxyArguments.apply(null, arguments));
}

export const slice = (obj, index = 0) => obj.length ? obj.slice ? obj.slice(index) : [].slice.call(obj, index) : [];

/**
 * Remove duplicates from array.
 *
 * @param   {object[]} arr     - array to be deduplicaed
 * @param   {boolean}  [force] - force deduplication of objects in array, i.e. [{foo: true}, {foo: true}] becomes [{foo: true}]
 * @returns {object[]} deduplicated array
 */
export function unique(arr, force) {
  const out = [];
  if (force) {
    var _arr = new Array(arr.length);
    each(arr, (elem, n) => {
      if (isObject(elem))
        _arr[n] = elem.toSource();
    });
  }
  for (let i = 0; i < arr.length; i++)
    if (force && isObject(arr[i]) ? _arr.indexOf(arr[i].toSource()) === i : arr.indexOf(arr[i]) === i)
      out.push(arr[i]);
  return out;
}

/**
 * Alias for unique
 * For more details @see unique.
 */
export const dedupe = unique;

/**
 * [[Description]]
 * @param   {[[Type]]} funcBody [[Description]]
 * @returns {[[Type]]} [[Description]]
 */
export function map(arr, callback) {
  const out = [];
  each(arr, (elem, n, arr) => {
    out.push(callback(elem, n, arr));
  });
  return out;
}

/**
 * [filter description]
 * @param  {[type]}   arr      [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
export function filter(arr, callback) {
  const out = [];
  each(arr, elem => {
    if (callback(elem))
      out.push(elem);
  });
  return out;
}

/**
 * [function description]
 * @param  {[type]} target [description]
 * @return {[type]}        [description]
 */
export const assign = O.assign || function(target) {
  if (!isObject(target))
    throw new TypeError("Cannot convert undefined or null to object");
  for (let i = 1; i < arguments.length; i++)
    each(arguments[i], (key, value) => {
      target[key] = value;
    });
  return target;
};

/**
 * [copy description]
 * @param  {[type]} from [description]
 * @return {[type]}      [description]
 */
export function copy(from) {
  if (A.isArray(from)) {
    return slice(from);
  } else if (isObjectLiteral(from)) {
    let target = {};
    each(from, (prop, value) => {
      target[prop] = value;
    }, true);
    return target;
  }
}

/**
 * [merge description]
 * @return {[type]} [description]
 */
export function merge() {
  const obj = {};
  for (let i = 0; i < arguments.length; i++)
    each(arguments[i], (key, val) => {
      obj[key] = val;
    });
  return obj;
}

/**
 * Empties arrays and objects (remove only writable properties).
 *
 * @param {object|object[]}  - object(s) to be emptied
 */
export function empty() {
  let i = 0;
  while (i < arguments.length) {
    let obj = arguments[i++];
    if (obj.length !== undefined && obj.pop)
      while (obj.pop());
    else if (isObjectLiteral(obj))
      each(obj, key => { delete obj[key]; }, true); // CAUTION: OBJECT MAY TURN INTO DICTIONARY MODE. IT REMOVES ONLY WRITABLE PROPERTIES.
  }
}

/**
 * Extends object.
 *
 * @param  {object} target - object to extend
 * @return {object} extended object
 */
export function extend(target) {
  for (let i = 1; i < arguments.length; i++)
    if (isObjectLiteral(arguments[i]))
      each(arguments[i], key => {
        O.defineProperty(target, key, O.getOwnPropertyDescriptor(arguments[i], key));
      }, true);
  return target;
}

/**
 *
 *
 * @return {[type]} [description]
 */
export function proxyArguments() {
  const args = new Array(arguments.length);
  for (let i = 0; i < args.length; i++)
    args[i] = arguments[i];
  return args;
}

/**
 * Binds 'this' and some extra arguments to function.
 *
 * @param  {function}  - the function to bind 'this' to.
 * @param  {any}       - value used as 'this'.
 * @param  {...args}   - extra params used as arguments to bound functions.
 * @return {function}  - bound function
 */
export function bind(funcToBind, thisArg) {
  if (typeof funcToBind !== "function")
    throw new TypeError("Bind must be called on a function");
  const theseArgs = new Array(arguments.length-2);
  if (theseArgs.length) {
    for (let i = 2, x = 0; i < arguments.length; i++)
      theseArgs[x++] = arguments[i];
  }
  return function() {
    if (arguments.length) {
      for (let i = 0, x = theseArgs.length; i < arguments.length; i++)
        theseArgs[x++] = arguments[i];
    }
    return funcToBind.apply(thisArg, theseArgs);
  };
}

export function destruct(object, method) {
  return object && typeof object[method] === "function" ? bind(object[method], object) : undefined;
}

/**
 * Detects whethere the given functions is native. That means it's built-in in the browser/engine etc.
 *
 * @param  {function} func - function to test
 * @return {boolean}
 */
export const isFunctionNative = function() { // Taken from https://github.com/Ginden/reflect-helpers/blob/master/reflect-helpers.js - thank you @Ginden
  const bind = F.prototype.bind && function() { // TODO: use tryCatch instead
          try {
            new F(`return (${F.prototype.bind})`);
            return false;
          } catch (e) {
            return true;
          }
        }();

  return function(func) {
    let sourceCode = F.toString.call(func);
    if (bind && sourceCode === F.toString.call(func.bind(null)))
      return true;
    if (sourceCode === "[function]")
      return true;
    return mayFail(sourceCode) === true;
  };
}();

/**
 * [mayFail description]
 * @param  {[type]}  func    [description]
 * @param  {Boolean} thisArg [description]
 * @param  {[type]}  ...args [description]
 * @return {[type]}          [description]
 */
export function mayFail(func, thisArg, ...args) {
  try {
    if (typeof func === "function")
      func.apply(thisArg, args);
    else if (typeof func === "string")
      new Function(`return ${func}`)().apply(thisArg, args);
    else
      throw new TypeError("Only functions and strings used as function constructor are allowed.");
    return true;
  } catch (ex) {
    log.console.error(func.toString(), ex);
    return ex;
  }
}

/**
 * Container for try catch clause. Useful for V8 as try-catch isn't an optimized pattern yet.
 *
 * @param  {function} _try   - function used as try
 * @param  {function} _catch - function used as catch
 * @return {any} value returned either by _try or _catch function
 */
export function tryCatch(_try, _catch) {
  try {
    return _try();
  } catch(ex) {
    if (typeof _catch === "function")
      return _catch(ex);
  }
}

/**
 * Try-catch clause for multiple function that stops on the first function with no error encountered.
 *
 * @param  {[type]} ...funcs [description]
 * @return {[type]}          [description]
 */
export function tryCatchMultiple(...funcs) {
  let failed = false;
  let ret;
  failed = every(funcs, func => tryCatch(() => {
    ret = func();
    return false;
  }, () => true));
  if (!failed)
    return ret;
}

/*jshint -W027, -W055 */
/**
 * [toFastProperties description]
 * @param  {[type]} obj [description]
 * @return {[type]}     [description]
 */
export function toFastProperties(obj) { // Credits to Petka!
  function f() {}
  f.prototype = obj;
  let l = 8;
  while (l--) new f();
  return;
  eval(obj);
}

/**
 * 'Blackhole' function that forces function to bailout.
 * Currently it isn't used anywhere yet it may be useful in future.
 */
export function blackHole() {
  try {} catch(e) {}
  return arguments;
  eval();
}
/*jshint +W027, +W055 */