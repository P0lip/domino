import { assign, contains, map } from "./core.es6";
import { Promise } from "./promises.es6";

class Storage {
  constructor(name, data, persistent) {
    this.storage = persistent ? window.localStorage : window.sessionStorage;
    if (!this.storage[name] && data) {
      this.storage[name] = JSON.stringify(data);
    }
    this.name = name;
  }

  /* call the constructor like Date for the specified value */
  get(Constructor) {
    const data = this.storage[this.name] ? JSON.parse(this.storage[this.name]) : undefined;
    return data && typeof Constructor === "function" ? new Constructor(data) : data;
  }

  has() {
    return contains(this.storage, this.name);
  }

  set(value) {
    this.storage[this.name] = JSON.stringify(value);
    return true;
  }

  get data() {
    return this.get();
  }

  set data(value) {
    return this.set(value);
  }

  remove() {
    this.storage.removeItem(this.name);
  }

  workOnParsed(callback) {
    const parsed = this.get();
    callback(parsed);
    this.set(parsed);
  }
}

export class SessionStorage extends Storage {
  constructor(name, data) {
    super(name, data, false);
  }
}

export class LocalStorage extends Storage {
  constructor(name, data) {
    super(name, data, true);
  }
}

export class Cookie {
  constructor(name) {
    this.name = name;
    this.regex = new RegExp(`(?:(?:^|.*;\\s*)${name}\\s*\\=\\s*([^;]*).*$)|^.*$`);
  }

  get() {
    return decodeURIComponent(document.cookie.replace(this.regex, "$1"));
  }

  remove() {
    this.set("", { expires: "Thu, 01 Jan 1970 00:00:00 GMT" });
    return true;
  }

  set(value, optional) {
    document.cookie = this.name + "=" + encodeURIComponent(value) + ` ;${map(optional, (option, value) => `${option}=${value}`).join(";")}`;
    return true;
  }
}

/* jshint -W030 */
window.indexedDB || (window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB);
window.IDBTransaction || (window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || { READ_WRITE: "readwrite" });
window.IDBKeyRange || (window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange);

const openDB = function() {
  const events = {};
  return (name, version) => events[name] || (events[name] = new Promise((resolve, reject) => {
    const request = window.indexedDB.open(name, version);
    request.onerror = event => {
      reject(event.target.errorCode);
    };
    request.onsuccess = event => {
      resolve(event);
    };
    request.onupgradeneeded = event => {
      resolve(event);
    };
    request.onversionchange = () => {
      request.close();
    };
  }));
}();

export function ExpirationError(message = "Database has expired") {
  this.name = "ExpirationError";
  this.message = message;
  this.stack = (new Error()).stack;
}

ExpirationError.prototype = Object.create(Error.prototype);
ExpirationError.prototype.constructor = ExpirationError;

export function MaximumAmount(message = "Maximum amount of entries reached") {
  this.name = "MaximumAmount";
  this.message = message;
  this.stack = (new Error()).stack;
}

MaximumAmount.prototype = Object.create(Error.prototype);
MaximumAmount.prototype.constructor = MaximumAmount;

export class IndexedDB {
  constructor({ name, table, keyPath, version = 1, expires = -1, max = -1 }) {
    if (!window.indexedDB)
      return;
    this.name = name;
    this.table = table;
    this.keyPath = keyPath;
    this.storage = new LocalStorage(`${IndexedDB.GUID}-${table}`, expires === -1 ? expires + Date.now() : expires);
    this._expires = expires;
    this.max = max;
    this.db = new Promise((resolve, reject) => {
      openDB(name)
        .then(event => {
          const db = event.target.result;
          if (event.type === "upgradeneeded") {
            const objectStore = db.createObjectStore(table, keyPath ? { keyPath } : { autoIncrement: true });
            IndexedDB.promisifyListeners(objectStore.transaction, () => {
              resolve(db);
            }, reject);
          } else
            resolve(db);
        })
        .catch(reject);
    });
    this.currentAmount = this.count();
  }

  static get GUID() {
    return "58bfb225-6b4f-48b4-895f-84abcfcc4f8f";
  }

  static promisifyListeners(obj, resolve, reject) {
    if ("oncomplete" in obj)
      obj.oncomplete = function(event) {
        resolve(event, event.target);
      };
    if ("onsuccess" in obj)
      obj.onsuccess = function(event) {
        resolve(event);
      };
    obj.onerror = function(event) {
      reject(event.target.error || event.target.errorCode);
    };
  }

  transaction(type = "readwrite") {
    return new Promise((resolve, reject) => {
      this.db
        .then(db => {
          resolve(db.transaction(this.table, type));
        })
        .catch(reject);
    });
  }

  store(type) {
    return new Promise((resolve, reject) => {
      return this.transaction(type)
        .then(transaction => {
          resolve(transaction.objectStore(this.table));
        })
        .catch(reject);
    });
  }

  add(data) {
    return new Promise((resolve, reject) => {
      if (this.expires)
        throw new ExpirationError(`Table ${this.table} has expired.`);
      if (this.keyPath && !contains(data, this.keyPath))
        throw new TypeError("No valid keyPath found");
      this.store()
        .then(store => {
          if (this.max < 0)
            IndexedDB.promisifyListeners(store.add(data), resolve, reject);
          else {
            this.currentAmount
              .then(amount => {
                if (this.max > 0 && this.max > amount)
                  IndexedDB.promisifyListeners(store.add(data), resolve, reject);
                else
                  throw new MaximumAmount();
              })
              .catch(reject);
            this.currentAmount = this.count();
          }
        })
        .catch(reject);
    });
  }

  count() {
    return new Promise((resolve, reject) => {
      this.store("readonly")
        .then(store => {
          IndexedDB.promisifyListeners(store.count(), function(event) {
            resolve(event.target.result);
          }, reject);
        })
        .catch(reject);
    });
  }

  get(id) {
    return new Promise((resolve, reject) => {
      this.store("readonly")
        .then(store => {
          IndexedDB.promisifyListeners(store.get(id), function({ target: { result }}) {
            if (!result)
              reject("Item not found");
            else
              resolve(result);
          }, reject);
        })
        .catch(reject);
    });
  }

  getAll() {
    return new Promise((resolve, reject) => {
      this.store("readonly")
        .then(store => {
          const data = [];
          IndexedDB.promisifyListeners(store.openCursor(), function(event) {
            const cursor = event.target.result;
            if (cursor) {
              data.push(cursor.value);
              cursor.continue();
            } else
              resolve(data);
          }, reject);
        })
        .catch(reject);
    });
  }

  update(data) {
    return new Promise((resolve, reject) => {
      if (this.expires)
        throw new ExpirationError(`Table ${this.table} has expired.`);
      this.get()
        .then((oldData, store) => {
          IndexedDB.promisifyListeners(store.put(assign(oldData, data)), resolve, reject);
        })
        .catch(reject);
    });
  }

  deleteDb(security) {
    return new Promise((resolve, reject) => {
      if (security === true)
        IndexedDB.promisifyListeners(window.indexedDB.deleteDatabase(this.name), resolve, reject);
      else
        reject("You must pass true to this function in order to remove the whole database.");
    });
  }

  deleteIndex() {

  }

  delete(id) {
    return new Promise((resolve, reject) => {
      this.store()
      .then(store => {
        IndexedDB.promisifyListeners(store.delete(id), function(event) {
          resolve(event.target.result);
        }, reject);
      })
      .catch(reject);
    });
  }

  clear() {
    return new Promise((resolve, reject) => {
      this.store()
        .then(store => {
          IndexedDB.promisifyListeners(store.clear(), resolve, reject);
        })
        .catch(reject);
    });
  }

  get expires() {
    return this._expires > 0 && Date.now() > this.storage.get();
  }

  set expires(newDate) {
    this.storage.set(newDate === true ? this._expires : newDate);
    return true;
  }
}