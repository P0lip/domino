/** @module Emitter */
import { WeakMap } from "./collections.es6";
import { contains, destruct, empty, isObject, each, mayFail } from "./core.es6";
import { ExtendedPromise } from "./promises.es6";

/**
 * Efficient Event Emitter allowing communication between objects in any modern (ES2015+) einviroment.
 * USE CASE: For instance you can replace the custom events in the browser with this Event Emitter. Just remember to pass window (or any object) as the target and make sure you use the same instance of the object.
 *
 * @constructor
 * @requires module:core
 * @param {object|function} [target={}]   target - Any object and function excluding null, used as a default event target.
 * @param {boolean}         [queue=false] queue - Indicates whether the events can be queue so that they can be used by further event listeners.
 */

export function Emitter(target, queue) {
  if (!isObject(target) && typeof target !== "function")
    throw new TypeError("Invalid value used as target");
  this.listeners = new WeakMap(); // the storage for objects and linked listeners
  this.target = target;
  if (queue)
    this.queue = new WeakMap();
}

/**
 * [function description]
 * @return {[type]} [description]
 */
Emitter.prototype.destruct = function() {
  const funcs = {};
  each(Emitter.prototype, method => {
    funcs[method] = destruct(this, method);
  });
  return funcs;
};

/**
 * [function description]
 * @param  {[type]} type [description]
 * @return {[type]}      [description]
 */
Emitter.prototype.dequeue = function(type) {
  if (!this.queue)
    return this;
  let queue = this.queue.get(this.target);
  if (queue && queue[type])
    each(queue[type], (type, events) => {
      empty(events);
    });
};

/**
 * [function description]
 * @param  {[type]} target   [description] // optional
 * @param  {string} type     [description]
 * @param  {function} listener [description]
 * @return {[type]}          [description]
 */
Emitter.prototype.delegate = function(type, listener) {
  if (this.queue) {
    let queued = this.queue.get(this.target);
    if (queued && queued[type]) {
      queued[type].forEach(function(entry) {
        mayFail(listener, listener, ...entry);
      });
    }
  }
  this.on(type, listener);
};

/**
 * [function description]
 * @param  {[type]} type    [description]
 * @param  {[type]} ...args [description]
 * @return {[type]}         [description]
 */
Emitter.prototype.emit = function(type, ...args) {
  const target = this.target;
  let listeners = this.listeners.get(target);
  if (listeners && listeners[type])
    each(listeners[type].slice(), listener => {
      mayFail(listener, listener, ...args);
    });
  if (this.queue) {
    let queue = this.queue.get(target);
    if (!queue || !queue[type]) {
      if (!queue)
        queue = {};
      queue[type] = [args];
      this.queue.set(target, queue);
    } else
      queue[type].push(args);
  }
};

/**
 * [function description]
 * @param  {[type]} type     [description]
 * @param  {[type]} listener [description]
 * @return {[type]}          [description]
 */
Emitter.prototype.off = function(type, listener) {
  let listeners = this.listeners.get(this.target);
  if (listeners && listeners[type])
    if (!listener)
      empty(listeners[type]);
    else {
      let index = listeners[type].indexOf(listener);
      while (index !== -1) {
        listeners[type].splice(index, 1);
        index = listeners[type].indexOf(listener);
      }
    }
};

/**
 * [function description]
 * @param  {[type]} type     [description]
 * @param  {[type]} listener [description]
 * @return {[type]}          [description]
 */
Emitter.prototype.on = function(type, listener) {
  const target = this.target;
  let listeners = this.listeners.get(target);
  if (!listeners || !listeners[type]) {
    if (!listeners)
      listeners = {};
    listeners[type] = [listener];
    this.listeners.set(target, listeners);
  } else if (listeners[type].max !== undefined ? listeners[type].max > listeners[type].length : true)
    listeners[type].push(listener);
};

/**
 * [function description]
 * @param  {[type]} type     [description]
 * @param  {[type]} listener [description]
 * @return {[type]}          [description]
 */
Emitter.prototype.once = function(type, listener) {
  let self = this;
  this.on(type, function _listener(...args) {
    mayFail(listener, listener, ...args);
    self.off(type, _listener);
  });
};

/**
 * [function description]
 * @param  {[type]} type     [description]
 * @param  {[type]} listener [description]
 * @return {boolean}
 */
Emitter.prototype.has = function(type, listener) {
  if (this.listeners.has(this.target)) {
    let listeners = this.listeners.get(this.target);
    return listener ? contains(listeners[type], listener) : contains(listeners, type);
  } else {
    return false;
  }
};

/**
 * Sets the maximum amount of listeners of given type.
 *
 * @param  {string} type - event type
 * @param  {number} n    - amount
 */
Emitter.prototype.setMaxListeners = function(type, n) {
  let listeners = this.listeners.get(this.target);
  if (!listeners || !listeners[type]) {
    if (!listeners)
      listeners = {};
    listeners[type] = [];
    listeners[type].max = n;
    this.listeners.set(this.target, listeners);
  } else {
    listeners[type].max = n < 0 ? undefined : n;
    if (n < 0)
      return;
    while (listeners[type].length > n)
      listeners[type].pop();
  }
};

/**
 * Takes entry from the queue if it's enabled. The entry stands for arguments passed earlier from emit.
 *
 * @param  {string}                    type - event
 * @param  {string|number} [n="first"] position
 * @return {array} entry
 */
Emitter.prototype.history = function(type, n = "first") {
  if (this.queue) {
    let queue = this.queue.get(this.target);
    if (queue && queue[type]) {
      queue = queue[type];
      return n === "first" ? queue[0] : n === "last" ? queue[queue.length-1] : queue[n];
    }
  }
};

/**
 * Creates a promise (no reject though) that will be resolved once the first event is fired or when there is an entry in the queue.
 *
 * @param  {string}               type - event type
 * @param  {boolean} [queue=true] indicates whether to try to take the last element from queue if it exists
 * @return {function} promise
 */
Emitter.prototype.promisify = function(type, queue = true) {
  return new ExtendedPromise(resolve => {
    const history = queue && this.history(type, "last");
    if (history)
      resolve(...history);
    else
      this.once(type, function(...args) {
        resolve(...args);
      });
  });
};