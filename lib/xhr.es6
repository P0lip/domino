/** @module xhr */
import { each, merge, tryCatch } from "./core.es6";
import { ExtendedPromise } from "./promises.es6";

export const parseHTML = function() {
  let Parser;
  tryCatch(function() {
    new DOMParser().parseFromString("", "text/html");
    Parser = markup => new DOMParser().parseFromString(markup, "text/html");
  }, function() {
    Parser = function(markup) {
      const doc = document.implementation.createHTMLDocument("");
      doc.documentElement.insertAdjacentHTML("afterbegin", markup);
      return doc;
    };
  });
  return markup => new Parser(markup);
}();

export const parseJSON = json => JSON.parse(json);

export const parseXML = xml => new DOMParser().parseFromString(xml, "application/xml");

export function request(url, { body = null, cache = true, credentials, form, headers = {}, method = "GET", mime, timeout, tryNumber = 3, tryDelay = 500 } = {}) {
  if (typeof url !== "string")
    throw new TypeError("Wrong URL.");
  let currentTry = 0;
  tryNumber = +tryNumber;
  return new ExtendedPromise(function request(resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open(method, cache ? url : url + "?" + Date.now(), true);

    xhr.addEventListener("load", () => {
      resolve(xhr.response, xhr);
    });

    xhr.addEventListener("loadend", function() {
      if (xhr.status === 0)
        reject("CORS");
    });

    xhr.addEventListener("error", () => {
      if (isNaN(tryNumber) || currentTry > tryNumber)
        reject(xhr.status + " " + xhr.statusCode, xhr);
      else
        window.setTimeout(function() {
          request(resolve, reject);
        }, isNaN(+tryDelay) ? 500 : +tryDelay);
    });

    if (!isNaN(+timeout))
      xhr.timeout = +timeout;

    if (typeof mime === "string")
      xhr.overrideMimeType(mime);

    if (headers)
      each(headers, (header, value) => {
        xhr.setRequestHeader(header, value);
      });

    xhr.withCredentials = !!credentials;

    if (form && !body)
      body = new FormData(body);

    xhr.send(body);
  });
}

export const get = request;

export const post = (url, setup) => request(url, merge(setup, { method: "POST" }));

export function getJSON(url, setup = {}) {
  return new ExtendedPromise(function(resolve, reject) {
    get(url, merge(setup, { mime: "application/json" }))
      .then(function(response, xhr) {
        resolve(parseJSON(response), xhr);
      })
      .catch(ex => {
        reject(ex);
      });
  });
}

export function getXML(url, setup = {}) {
  return new ExtendedPromise(function(resolve, reject) {
    get(url, merge(setup, { mime: "application/xml" }))
      .then(function(response, xhr) {
        resolve(parseXML(response), xhr);
      })
      .catch(ex => {
        reject(ex);
      });
  });
}

export function getHTML(url, setup = {}) {
  return new ExtendedPromise(function(resolve, reject) {
    get(url, merge(setup, { mime: "text/html" }))
      .then(function(response, xhr) {
        resolve(parseHTML(response), xhr);
      })
      .catch(ex => {
        reject(ex);
      });
  });
}