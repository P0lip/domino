/* jshint devel: true, -W064 */
// https://code.google.com/p/v8/source/browse/branches/bleeding_edge/src/runtime.h?r=22913


export const isNativeSyntaxSupported = function() {
  try {
    eval("%GetV8Version()");
    return true;
  } catch (e) {
    return false;
  }
}();

const evaluate = isNativeSyntaxSupported ? (funcName, ...args) => Function(`return %${funcName}(...args)`)() : () => "Syntax unsupported";

/* Functions utils */
export const GetOptimizationStatus = func => evaluate("GetOptimizationStatus", func);

export const OptimizeFunctionOnNextCall = func => evaluate("OptimizeFunctionOnNextCall", func);

export const FunctionGetSourceCode = func => evaluate("FunctionGetSourceCode", func);

//FunctionIsArrow, FunctionIsGenerator, FunctionIsAPIFunction

export const GetOptimizationAvailability = isNativeSyntaxSupported ? function(func, self, ...args) {
  func.apply(self, args);
  func.apply(self, args);
  OptimizeFunctionOnNextCall(func);
  func.apply(self, args);
  const result = GetOptimizationStatus(func) === 1;
  console.assert(result === true, `${func} isn't optimizable.`);
  return result;
} : () => "Syntax unsupported";

/* Object utils */

export const HasFastSmiElements = array => evaluate("HasFastSmiElements", array);

export const HasFastHoleyElements = array => evaluate("HasFastHoleyElements", array);

export const HasDictionaryElements = obj => evaluate("HasDictionaryElements", obj);

export const HasFastProperties = obj => evaluate("HasFastProperties", obj);


export const NewString = (len, oneByte) => evaluate("NewString", len, oneByte);