/* global console */
import { DEBUG, name } from "../data/config.es6";

const lineNumber = /\d[\d:]+$/;

export function log(...msg) {
  if (DEBUG) {
    const stack = (new Error).stack.split("\n");
    let callerLine = "";
    if (stack.length && stack[2] && lineNumber.test(stack[2]))
      callerLine = stack[2].match(lineNumber)[0];
    if (typeof msg[0] === "string" && msg[0].indexOf("%c") === 0) {
      console.log.call(console, name + " " + msg[0], msg[1], callerLine);
    } else {
      console.log(...msg, callerLine);
    }
  }
}

log.success = function(msg) {
  log(`%c${msg}`, "color: white; background-color: green; padding: 2px");
};

log.error = function(msg) {
  log(`%c${msg}`, "color: white; background-color: red; padding: 2px");
};

log.info = function(msg) {
  log(`%c${msg}`, "color: white; background-color: #0099e5; padding: 2px");
};

log.warn = function(msg) {
  log(`%c${msg}`, "color: black; background-color: yellow; padding: 2px");
};

log.console = typeof Proxy === "function" ? new Proxy(console, {
  get(target, method) {
    return DEBUG ? target[method].bind(target) : function(){};
  }
}) : function() {
  const methods = {};
  for (let method in console)
    methods[method] = function(...args) {
      if (DEBUG)
        console[method](...args);
    };
  return methods;
}();