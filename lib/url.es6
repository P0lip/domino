/** @module url */
import { contains, assign } from "./core.es6";

export function formatURL(url = window.location.href) {
  if (url.indexOf("://") === -1)
    throw new TypeError(`${url} is not a valid URL.`); // it's a very naive approach to check the correctness, but it's cheap
  let credientals = contains(url, "@") && contains(url, ":");
  if (credientals) {
    credientals = formatURL.regex.user.exec(url);
    url = url.replace(new RegExp(`${credientals[1]}:${credientals[2]}@`, ""), "");
  }
  let split = url.split("/");
  let protocol = split[0],
      host = split[2],
      hostname = host,
      port = "",
      origin = `${protocol}//${host}`,
      query = "";

  if (contains(host, ":")) {
    let split2 = host.split(":");
    port = split2[1];
    hostname = split2[0];
  }

  query = contains(url, "?") && formatURL.regex.query.test(url) ? formatURL.regex.query.exec(url)[1] : "";


  const pieces = {
    hash: contains(url, "#") ? formatURL.regex.hash.exec(url)[0] : "",
    host,
    hostname,
    href: url,
    origin,
    password: credientals ? credientals[2] : "",
    //pathname: url.replace(origin, ""),
    pathname: "/" + split.slice(3).join("/"),
    port,
    protocol,
    searchParams: Object.freeze({
      get(name) {
        return query.length ? function () {
          var regex = new RegExp(name + "=([^&]+)");
          return regex.test(query) ? query.match(regex)[1] : "";
        }() : "";
      },
      set(name, value) {
        const queryValue = this.get(name);
        if (!queryValue.length) {
          const str = (query.length ? "&" : "?") + `${name}=${value}`;
          query += str;
          pieces.href += str;
          pieces.pathname += str;
          return true;
        }
        const reg = new RegExp(`name=([^&]${queryValue}+)`);
        query = query.replace(reg, `${name}=${value}`);
        pieces.href = pieces.href.replace(reg, `${name}=${value}`);
        pieces.href = pieces.pathname.replace(reg, `${name}=${value}`);
        return true;
      }
    }),
    username: credientals ? credientals[1] : "",
    secure: protocol === "https:",
    toString() {
      return this.href;
    }
  };

  return pieces;
}

window.formatURL = formatURL;

formatURL.regex = {
  hash: /#(.+)$/,
  host: /^\w+:\/\/(.[^\/]+)/,
  protocol: /^\w+(?=:)/,
  port: /:(\d+)(?=\/|$)/,
  user: /\/+(.+):(.+)(?=@)/,
  query: /\?(.+)/
};

formatURL.paths = function(url = window.location.href) {
  let parsed = formatURL(url),
      currentPath = "";

  return assign(parsed, {
    pathnames: parsed.pathname.split("/").slice(1).map(path => currentPath += "/" + path)
  });
};