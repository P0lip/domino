import { createSimpleIterator, definePrivateProperty, each, isObject, isObjectLiteral, generateGUID } from "./core.es6";

export const collections = {
  removable: false // as default we **don't delete** items to prevent the object entering the dictionary mode, so it **MAY CAUSE A MEMORY LEAK**.
};
export default collections;

export const WeakMap = collections.WeakMap = window.WeakMap || class {
  constructor(iterable, removable) {
    this.GUID = generateGUID();
    if (iterable && iterable.length) {
      for (let i = 0; i < iterable.length; i++) {
        let iterable = iterable[i];
        this.set(iterable[0], iterable[1]);
      }
    }
    if (iterable === true || collections.removable || removable === true)
      this.removable = true;
  }
  delete(key) {
    if (this.removable) {
      if (this.GUID in key) {
        delete key[this.GUID];
        return true;
      }
      return false;
    }
    if (this.GUID in key) {
      key[this.GUID] = this.GUID;
      return true;
    }
    return false;
  }
  get(key) {
    return key[this.GUID] === this.GUID ? undefined : key[this.GUID];
  }
  has(key) {
    return this.GUID in key && key[this.GUID] !== this.GUID;
  }
  set(key, value) {
    if (!isObject(key) && typeof key !== "function")
      throw new TypeError("Invalid value used as weak map key");
    if (this.GUID in key)
      key[this.GUID] = value;
    else
      definePrivateProperty(key, this.GUID, value);
    return this;
  }
};

export const Map = collections.Map = window.Map || class {
  constructor(iterable, removable) {
    this.storage = {}; // may drop to the hash table after inserting a bunch of elements with different types (mainly "slow" types)
    this.size = 0;
    this.order = []; // above applies aswell
    WeakMap.constructor.call(this, iterable, removable);
  }
  clear() {
    this.forEach((value, key) => {
      this.delete(key);
      this.order.pop(); // we can safely do it due to the implementation of forEach function
    });
  }
  entries() {
    const iterator = createSimpleIterator(this.order);
    return {
      next: () => {
        const { value, done } = iterator.next();
        return {
          value: value ? [value[0], this.storage[value[0]]] : undefined,
          done
        };
      }
    };
  }
  keys() {
    const entries = this.entries();
    return {
      next() {
        const { value, done } = entries.next();
        return {
          value: value ? value[0] : undefined,
          done
        };
      }
    };
  }
  forEach(callback, thisArg) {
    if (this.order.length) {
      const iterator = createSimpleIterator(this.order);
      let key = iterator.next();
      do {
        callback.call(thisArg, this.storage[key.value], key.value, this);
        key = iterator.next();
      } while (!key.done);
    }
  }
  delete(key) {
    if (this.removable) {
      if (this.storage[key]) {
        delete this.storage[key];
        return true;
      }
      return false;
    }
    if (key in this.storage) {
      this.storage[key] = this.GUID; // as default we **don't delete** items to prevent the object entering the dictionary mode, so it **MAY CAUSE A MEMORY LEAK**.
      this.order.splice(key, 1);
      this.size--;
      return true;
    }
    return false;
  }
  get(key) {
    return this.storage[key] === this.GUID ? undefined : this.storage[key];
  }
  has(key) {
    return key in this.storage && this.storage[key] !== this.GUID;
  }
  set(key, value) {
    this.order.push(key);
    this.storage[key] = value;
    this.size++;
    return this;
  }
  values() {
    const entries = this.entries();
    return {
      next() {
        const { value, done } = entries.next();
        return {
          value: value ? value[1] : undefined,
          done
        };
      }
    };
  }
};

export const _WeakSet = collections.WeakSet = window.WeakSet || class {
  constructor(iterable, removable) {
    this.GUID = generateGUID();
    if (iterable && iterable.length) {
      for (let i = 0; i < iterable.length; i++)
        this.add(iterable[i]);
    }
    if (iterable === true || collections.removable || removable === true)
      this.removable = true;
  }
  add(value) {
    if (!isObject(value) && typeof value !== "function")
      throw new TypeError("Invalid value used.");
    if (this.GUID in value)
      value[this.GUID] = true;
    else
      definePrivateProperty(value, this.GUID, true);
  }
  delete(value) {
    if (this.removable) {
      if (this.GUID in value) {
        delete value[this.GUID];
        return true;
      }
      return false;
    }
    if (this.GUID in value) {
      value[this.GUID] = false; // as default we **don't delete** items to prevent the object entering the dictionary mode, so it **MAY CAUSE A MEMORY LEAK**.
      return true;
    }
    return false;
  }
  has(value) {
    return this.GUID in value ? value[this.GUID] === true : false;
  }
};

export const Set = collections.Set = window.Set || class {
  constructor(iterable) {
    this.storage = [];
    if (typeof iterable === "string")
      this.add(iterable[0]);
    else if (!isObjectLiteral(iterable) && iterable.length)
      each(iterable, iterated => {
        this.add(iterated);
      });
  }
  add(value) {
    if (!this.has(value))
      this.storage.push(value);
    return this;
  }
  clear() {
    while (this.storage.length)
      this.storage.pop();
  }
  delete(value) {
    if (!this.has(value))
      this.storage.splice(this.storage.indexOf(value), 1);
    return false;
  }
  entries(optional) {
  //  return collections.Map.prototype.entries.call(this, optional);
  }
  has(value) {
    return this.storage.indexOf(value) !== -1;
  }
  keys() {
    return this.entries("keys");
  }
  values() {
    return this.entries("values");
  }
};