import { Promise } from "./promises.es6";
import { bind, every, slice } from "./core.es6";

const documentElement = document.documentElement,
      documentElementStyle = documentElement.style,
      createElement = (type, parent = document.body || documentElement) => parent === null ? document.createElement(type) : parent.appendChild(document.createElement(type)),
      UA = navigator.userAgent,
      engine = !!window.chrome && !documentElementStyle.msTouchAction ? "Chromium" : documentElementStyle.WebkitAppearance !== undefined ? "Webkit" : documentElementStyle.MozAppearance !== undefined ? "Gecko" : ({}).toString.call(window.opera) == "[object Opera]" ? "Presto" : documentElementStyle.msTouchAction !== undefined || (document.all && !window.atob )? "Trident" : undefined;

export const prefix = (/-(moz|webkit|ms)-/.exec(slice(window.getComputedStyle(documentElement, null)).toString()) || (window.opera && window.opera.version() >= 10.60 ? [,"o"] : []))[1];

export const CSSPrefix = `-${prefix}-`;

export const CSSFeatures = {
  prefix,
  CSSPrefix,

  /**
   * Tests whether an at (@) rule is supported.
   * NOTE: All @ rules are written on MDN.
   *
   * @param   {string} atRule
   * @returns {boolean}
   */
  isAtRuleSupported(atRule) { // TODO: use exposed API like window.CSSFontFaceRule
    let styleElem = createElement("style"),
        styleSheet = styleElem.sheet || styleElem.styleSheet;
    styleSheet.addRule(atRule);
    let matched = styleSheet.cssRules.length === 1;
    styleElem.parentNode.removeChild(styleElem);
    return matched;
  },

  /**
   * Detects the support of given CSS rule.
   *
   * @param   {string}  prop - CSS rule's property
   * @param   {string}  value - CSS rule's value
   * @param   {boolean|string} [prefix] - Prefix - If true is passed, it prepends the CSS rule's property with the detected vendor prefix, otherwise if a string is given, it adds a passed prefix to the rule's property beginning.
   * @returns {boolean}
   */
  isFeatureSupported: window.CSS && window.CSS.supports || window.supportsCSS ? function() {
    const supports = bind(window.supportsCSS || window.CSS.supports, window.CSS || window);
    return (prop, value, prefix) => supports(prop, value) || (prefix ? typeof prefix === "string" ? supports(`${prefix}${prop}`, value) : supports(`${CSSPrefix}${prop}`, value) : false);
  }() : function() {
    const element = document.createDocumentFragment().appendChild(document.createElement("div")),
        check = prop => element.style[prop] !== undefined && element.style[prop] !== "";
    return function(prop, value, prefix) {
      element.style[prop] = value;
      if (prefix)
        element.style[typeof prefix === "string" ? `${prefix}prop` : `${prefix = CSSPrefix}${prop}`] = value;
      return check(prop) || prefix ? check(`${prefix}${prop}`) : false;
    };
  }(),

  /**
   * Checks whether @media rules can affect (under defined conditions like max-height, max-width etc.) the layout.
   * NOTE: The valid argument contains media rules without the @media prefix. Example: only screen and (max-width: 1000px) etc. Just pass the string without @media, leave the rest.
   *
   * @param   {string}  rules - @media rules
   * @returns {boolean}
   */
  areMediaRulesMatching: window.matchMedia ? rules => window.matchMedia(rules).matches : function(rules) {
    const styleElem = createElement("style"),
        p0Elem = createElement("div");
    p0Elem.className = "-p0lip-";
    (styleElem.sheet || styleElem.styleSheet).addRule(`media ${rules}`, "div.-p0lip-{ display:none;width:0px;height:0px }");
    const p0ElemStyle = window.getComputedStyle(p0Elem, null);
    styleElem.parentElement.removeChild(styleElem);
    p0Elem.parentElement.removeChild(p0Elem);
    return p0ElemStyle.getPropertyValue("display") === "none" && p0ElemStyle.getPropertyValue("width") === "0px" && p0ElemStyle.getPropertyValue("height") === "0px";
  }
};

export const device = {
  browser: engine,
  version: function() {
    let regexp;
    switch (engine) {
      case "Chromium":
        regexp = /Chrome\/(\d+)/;
        break;
      case "Gecko":
        regexp = /Firefox\/(\d+)/;
        break;
      case "Webkit":
        regexp = /Version\/(\d+)/;
        break;
      case "Trident":
        return document.addEventListener && !window.atob ? 9 : window.atob && document.documentMode === 10 ? 10 : 11;
      case "Presto":
        return +window.opera.version();
    }
    return regexp && regexp.test(UA) ? +regexp.exec(UA)[1] : undefined;
  }(),

  orientation: "landscape",

  type: function() {
    if (!this.touchable)
      return "desktop";
    const createMediaRule = (width, height, ratio) => {
      const prefix = `only screen and (max-width:${width}px) and (max-height:${height}px) and`;
      return `${prefix} max-resolution:${ratio}ddpx), ${prefix} -webkit-max-device-pixel-ratio:${ratio}), ${prefix} max--moz-device-pixel:${ratio}), -o-device-pixel-ratio:${ratio})`;
    };
    const mediaRules = {
      smartphone: [[360, 640, 1], [360, 640, 1.5], [400, 695, 2], [320, 480, 2.4], [360, 640, 4]],
      phablet: [[320, 480, 2.22], [414, 736, 3.5]],
      tablet: [[800, 1366, 1], [800, 1280, 1.5], [800, 1280, 2]]
    };

    let _device = "desktop";

    every(mediaRules, (device, rules) => {
      if (_device === "desktop")
        every(rules, dimensions => {
          if (CSS.testMediaRule(createMediaRule(dimensions))) {
            _device = device;
            return false;
          } else if (CSS.testMediaRule(createMediaRule(dimensions[1], dimensions[0], dimensions[2]))) {
            _device = device;
            this.orientation = "portrait";
            return false;
          }
        });
      else
        return false;
    });

    return _device;
  }(),

  /* A bit naive assumption */
  is64CPU: navigator.hardwareConcurrency ? navigator.hardwareConcurrency >= 4 : navigator.oscpu ? /Windows NT[6-9]\.[0-9]; (?=Win64;\sx64$|WOW64$)|Linux (?=x86_64$|i686 on x86_64$)|Mac OS/.test(navigator.oscpu) : /Windows\sNT *[6-9]\.[0-9]; (?=Win64; x64|WOW64)|(FreeBSD|Linux) *(?=x86_64|i686 on x86_64|AMD64|amd64)|Mac OS/.test(UA),

  isTouchable: "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch || CSS.areMediaRulesMatching("(touch-enabled:1)", `(${CSSPrefix}touch-enabled:1)`),

  isOperaMini: window.operamini && ({}).toString.call(window.operamini) === "[object OperaMini]"
};

const unprefixedAnimation = CSS.isFeatureSupported("animation", "x 1s");

export const events = {
  isSupported: (elem, eventType) => `on${eventType}` in elem,
  transitionEnd: CSS.isFeatureSupported("transition", "all 1s") || prefix === "moz" || prefix === "ms" ? "transitionend" : documentElementStyle.WebkitAppearance ? "webkitTransitionEnd" : engine === "Presto" && device.version >= 12.10 ? "transitionend" : engine === "Presto" && device.version >= 12.0 && device.version < 12.10 ? "otransitionend" :  "oTransitionEnd",

  animationStart: unprefixedAnimation || prefix === "moz" ? "animationstart" : prefix === "o" ? "oanimationstart" : (prefix === "ms" ? prefix.toUpperCase() : prefix) + "AnimationStart",
  animationIteration: unprefixedAnimation || prefix === "moz" ? "animationiteration" : prefix === "o" ? "oanimationiteration" : (prefix === "ms" ? prefix.toUpperCase() : prefix)  + "AnimationIteration",
  animationEnd: unprefixedAnimation || prefix === "moz" ? "animationend" : prefix === "o" ? "oanimationend" : (prefix === "ms" ? prefix.toUpperCase() : prefix) + "AnimationEnd"
};

/**
 * Determinates the support of particular image type.
 *
 * @param   {string}    link - URI to the image
 * @param   {function}  callback - function called with the result of test
 * @returns {promise}  promise
 */
export function isImageTypeSupported(link) {
  return new Promise(function(resolve, reject) {
    let image = new Image();
    image.onload = image.onerror = () => {
      if (image.width || image.height)
        resolve();
      else
        reject();
    };
    image.src = link;
  });
}

/**
 * Reveal the availability of use the given tag name.
 *
 * @param   {string}  tag - HTML tag
 * @returns {boolean}
 */
export const isElementSupported = tag => !(document.createElement(tag) instanceof window.HTMLUnknownElement);

/**
 * Detects the possibilty to use some attribute.
 *
 * @param   {string}  attr - HTML attribute
 * @param   {string}  tag - HTML tag to test on
 * @returns {boolean}
 */
export const isAttributeSupported = (attr, tag) => !(attr in document.createElement(tag));