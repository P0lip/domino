//TODO:
// auto direction

"use strict";
import Core from "./core";
import { CSS, events } from "./feature.es6";
import $ from "./DOM";
import { easings } from "./CSS.es6";
const support3D = CSS.isFeatureSupported("transform", "translate3d(0,0,0)", true),
      supportWillChange = CSS.isFeatureSupported("will-change", "scroll, top");
class Scroll {
  constructor (container, { direction = "vertical", easing = "random", duration = 1500, infinitive = true, beforeMove, afterMove, keys = true, scroll = true, arrows = true, touch = false } = {}) {
    if (!container)
      return;

    const $container = this.container = $(container);
    if ($container.isEmpty)
      return;

    this.emitter = new Emitter(this, false);

    const $window = this.window = $window(),
          $document = this.document = $(document),
          $sections = this.sections = $container.children("section, div"),
          $current = this.current = $sections.first();
    this.containerParent = $container.parent();
    this.duration = duration;
    this.direction = direction;
    this.infinitive = infinitive;


    this.beforeMove = beforeMove;
    this.afterMove = afterMove;
    this.isScrolling = false;
    this.index = 0;
    this.lastIndex = $sections.length-1;

  }

  get next() {
    return this.sections.eq(this.index+1);
  }

  get prev() {
    return this.sections.eq(this.index-1);
  }

}



  const
        wheelEvent = events.isSupported("wheel") ? "wheel" : "mousewheel",
        fixScroll = direction === "vertical" ? () => window.scrollTo(0, $current.offset.top) : () => window.scrollTo($current.offset.left, 0),
        scrollCallback = () => {},
        universalCallback = e => {
          if (this.isScrolling) {
            e.returnValue = false;
            return e.preventDefault();
          }
          switch (e.type) {
            case "click":
              return e.target === arrows.first().get()[0] ? this.scrollNext(e) : e.target === arrows.last().get()[0] ? this.scrollPrev(e) : null;
            case "keydown":
              let key = e.key || e.code || e.which || e.keyCode || e.which;
              if (direction === "vertical")
                switch (key) {
                  case "ArrowUp":
                  case "PageUp":
                  case 33:
                  case 38:
                    return this.scrollPrev(e);
                  case "ArrowDown":
                  case "PageDown":
                  case 32:
                  case 34:
                  case 40:
                    return this.scrollNext(e);
                  case "Home":
                    return this.scrollTo(0, e);
                  case "End":
                    return this.scrollTo(this.lastIndex, e);
                }
              else
                switch (key) {
                  case 37:
                    return this.scrollPrev(e);
                  case 39:
                    return this.scrollNext(e);
                }
              break;
            case wheelEvent:
              if ((e.deltaY || e.wheelDeltaY) > 0)
                 this.scrollNext(e);
              else if ((e.deltaY || e.wheelDeltaY) < 0)
                this.scrollPrev(e);
          }
        },
        listenOrMute = toggle => {
          if (scroll)
            $window[toggle](wheelEvent, universalCallback);
          if (keys)
            $document[toggle]("keydown", universalCallback);
          if (arrows) {
            arrows[toggle]("click", universalCallback);
            arrows.show();
          }
        };
  let overflowState = this.isScrollable;
  if (arrows) {
    let arrowsContainer = $.createElement(document.body, "nav", {
      id: "scrollNav"
    });
    arrows = $(
      [$.createElement(arrowsContainer, "li", {
        className: "scrollNavLi",
        style: {
          display: "none"
        }
      }),
       $.createElement(arrowsContainer, "li", {
         className: "scrollNavLi",
         style: {
          display: "none"
         }
       })]
    );
  }
  if (!overflowState)
    listenOrMute("on");

  $window.on("resize", () => {
    var newOverflowState = this.isScrollable;
    if (!newOverflowState)
      fixScroll();
    if (newOverflowState !== overflowState) {
      $document.trigger(overflowEvent);
      overflowState = newOverflowState;
      listenOrMute(overflowState ? "off" : "on");
    }
  });

  let paused = false;
  $window.on("scroll", () => {
    if (!paused) {
      paused = true;
      window.setTimeout(() => {
        paused = false;
        $window.trigger(scrollEvent);
      }, this.duration/10);
    }
  });

  $window.on("_scroll", () => {
    if (this.isScrolling)
      this.isScrolling = false;
    let current = this.current = $sections.filterVisible().first();
    current.trigger(visibleEvent);
    if (arrows)
      arrows.show();
    if (current.isEmpty) {
      //let current = self.currentElem = self[direction];
      //current.dispatchEvent(visibleEvent);

      if (overflowState)
        this.currentElem = "";

    }
  });
};



  get isScrollable() {
    return this.containerParent.css("overflow-" + this.direction === "vertical" ? "y" : "x") !== "hidden";
  },

  scroll(directionOrIndex, e) {
    if (!directionOrIndex)
      return;
    if (e && this.container.isEvent(e)) {
      e.returnValue = false;
      e.preventDefault();
    }
    const next = !this.sections.eq(typeof index === "number" ? index : this.index+1).isEmpty,
          prev = !this.sections.eq(typeof index === "number" ? index : this.index-1).isEmpty;
    let section;
    if (typeof directionOrIndex === "string") {
      if (directionOrIndex === "next") {
        if (canScroll.next)
          section = this.next;
        else if (this.infinitive)
          section = this.sections.first();
      } else if (directionOrIndex === "prev") {
        if (canScroll.prev)
          section = this.prev;
        else if (this.infinitive)
          section = this.sections.last();
      }
    } else if (typeof directionOrIndex === "number")
      section = this.sections.eq(directionOrIndex);

    if (!section | section.isEmpty)
      return;
    this.index = this.sections.index(section);
    if (!this.isScrolling)
      this.isScrolling = true;
    if (typeof this.internalBefore === "function")
      this.internalBefore(section);
    if (typeof this.beforeMove === "function")
      this.beforeMove(section);

    const offset = [this.direction === "vertical" ? "top" : "left"],
          sectionOffset = section.offset[offset],
          windowOffset = this.window.offset[offset],
          difference = this.direction === "next" ? Math.abs(sectionOffset - windowOffset) : -Math.abs(sectionOffset - windowOffset);
    this.container.animate({
      transform: support3D ? this.direction === "vertical" ? `translate3d(0, ${difference}px, 0)` : `translate3d(${difference}px, 0, 0)` : `translate${this.direction === "vertical" ? "Y" : "X"}(${difference}px)`
    }, {
      duration: this.duration,
      easing: this.easing,
      callback: () => {
        this.isScrolling = false;
        if (typeof this.afterMove === "function")
          this.afterMove.call(this, section);
        if (typeof this.internalAfter === "function")
          this.internalAfter(section);
        this.container.revert();
      }
    });
  },
  scrollTo(index, e) {
    this.scroll(index);
  },
  scrollNext(e) {
    this.scroll("next", e);
  },
  scrollPrev(e) {
    this.scroll("prev", e);
  }
});