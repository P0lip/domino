import $ from "../lib/DOM.es6";
import { forceReflow } from "../lib/CSS.es6";

export function matchHeight(selector, height) {
   const elems = $(selector);
   if (!height) {
     elems.each(elem => {
       forceReflow(elem);
       let _height = $(elem).outerHeight();
       if (_height > height)
         height = _height;
     });
   }
   elems.height(height);
 }