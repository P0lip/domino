const gulp = require("gulp"),
      rollup = require("gulp-rollup-babel"),
      plumber = require("gulp-plumber"),
      dest = require("gulp-dest"),
      sourcemaps = require("gulp-sourcemaps"),
      gutil = require("gulp-util");

gulp.task("build", () => {
  return gulp.src("lib/DOM.es6")
    .pipe(sourcemaps.init())
    .pipe(plumber(function(error) {
      gutil.log(error);
      this.emit("end");
    }))
    .pipe(rollup({
      format: "iife",
      moduleName: "DOMino",
      sourceMap: true,
      intro: "var win = window, doc = document;"
    }))
    //.pipe(rename("rapi"))
    .pipe(dest("./build", { ext: ".js" }))
    .pipe(gulp.dest("./"))
    // .pipe(uglify({
    //   outSourceMap: true
    // }))
    //.pipe(dest("./", { ext: ".min.js" }))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./"));
});


gulp.task('watch', function() {
  gulp.watch(["lib/*es6", "data/*es6"], ["build"]);
});
//gulp.task("test");